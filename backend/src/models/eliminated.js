import { Schema, model } from 'mongoose';

const eliminatedSchema = new Schema(
    {
        hit_id:String
    }
);

export default model('eliminatedModel', eliminatedSchema);