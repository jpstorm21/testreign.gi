import { Schema, model } from 'mongoose';

const hitSchema = new Schema(
    {
        object_id: String,
        title: String,
        url: String,
        author: String,
        created_at: String
    }
);

export default model('hitModel', hitSchema);