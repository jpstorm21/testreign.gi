import express from 'express';
import cors from "cors";
import morgan from 'morgan';
import cron from 'node-cron';
import hitRoutes from './routes/hit.routes';
import { loadData } from './helpers/helpers';
import './helpers/connectionMongoose';
import dotenv from 'dotenv';

const app = express();

dotenv.config();

app.set("port", process.env.PORT || 4000);

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());

//routes API
app.use('/api', hitRoutes);

cron.schedule('* * */1 * *', () => {
    loadData();
});

app.use('/', (req, res) => {
    res.send('API EXPRESS');
});

app.listen(app.get("port"), () => {
    console.log(`Server on port ${app.get("port")}`);
});

export default app;