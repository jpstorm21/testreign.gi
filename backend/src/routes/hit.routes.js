import { Router } from "express";
import hitCtrl from "../controllers/hit.controller";

const router = Router();

// GET
router.get("/hits", hitCtrl.getHits);

//DELETE
router.delete('/hit/:id', hitCtrl.deleteHit)

// ALL
router.all("*", (req, res) => {
  res.status(404).json({
    message: "La ruta de la solicitud HTTP no es reconocida por el servidor.",
  });
});

export default router;
