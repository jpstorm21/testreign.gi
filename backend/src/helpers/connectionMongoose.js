import mongoose from 'mongoose'

const MONGO_NAME = process.env.MONGO_NAME || 'testReign';
const MONGO_HOST = process.env.MONGO_HOST || 'localhost';

export const URI = process.env.MONGOOSE_URI || `mongodb://${MONGO_HOST}/${MONGO_NAME}`;

mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('Database is connected');
});