import axios from "axios";
import hitModel from "../models/hit";
import eliminatedModel from "../models/eliminated";

export const loadData = async () => {
  await hitModel.remove({});
  const { status, data } = await axios.get(
    "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
  );

  if (status === 200) {
    const { hits } = data;
    if (Array.isArray(hits) && hits.length > 0) {
      const eliminateds = await eliminatedModel.find();
      hits.forEach(async (hit) => {
        const {
          created_at,
          title,
          url,
          author,
          story_title,
          story_url,
          objectID,
        } = hit;

        if (title || story_title) {
          const eliminated = eliminateds.find(
            (eliminated) => eliminated.hit_id === objectID
          );

          if (!eliminated) {
            const hit = new hitModel({
              title: title || story_title,
              url: url || story_url,
              object_id: objectID,
              author,
              created_at,
            });

            await hit.save();
          }
        }
      });
    }
  }
};

export const simulateDeleteHit = (id) => {
  let aux = [];

  /* array that simulate a find in hitModels (
        const hit = await hitModel.findOne({object_id: id});
      ) */
  const hits = [
    {
      _id: "5f62f1108296e2229c6d0bcd",
      title: "VMware Cuts Pay for Remote Workers Fleeing Silicon Valley",
      url:
        "https://finance.yahoo.com/news/vmware-twitter-cut-pay-remote-184722278.html",
      object_id: "24501200",
      author: "amscanne",
      created_at: "2020-09-17T04:31:39.000Z",
    },
  ];

  const hit = hits.find((hit) => hit.object_id === id);

  if (!hit) {
    return {
      status: 404,
      msg: "Hit no existe",
    };
  }

  /* simulate save in bd
        const hitEliminated = new eliminatedModel({ hit_id: hit.object_id });
        await hitEliminated.save();
    */
  aux.push(hit);

  /* simulate delete in bd
       await hitModel.findByIdAndDelete(hit._id);
    */
  aux.filter((x) => x !== hit._id);

  return {
    status: 200,
    msg: "hit eliminado exitosamente",
  };
};
