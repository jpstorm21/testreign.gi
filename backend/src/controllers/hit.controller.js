import hitModel from "../models/hit";
import eliminatedModel from "../models/eliminated";

const hitCtrl = {};

hitCtrl.getHits = async (req, res) => {
  try {
    const data = await hitModel.find().sort({ created_at: -1 });
    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json({
      msg: "Ocurrió un error en el servidor.",
      error,
    });
  }
};

hitCtrl.deleteHit = async (req, res) => {
  try {
    const { id } = req.params;

    const hit = await hitModel.findOne({ object_id: id });

    if (!hit) {
      return res.status(404).json({
        msg: "Hit no existe",
      });
    }

    const hitEliminated = new eliminatedModel({ hit_id: hit.object_id });
    await hitEliminated.save();

    await hitModel.findByIdAndDelete(hit._id);

    return res.status(200).json({
      msg: "hit eliminado exitosamente",
    });
  } catch (error) {
    return res.status(500).json({
      msg: "Ocurrió un error en el servidor.",
      error,
    });
  }
};

export default hitCtrl;
