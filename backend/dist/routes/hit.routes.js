"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _hit = _interopRequireDefault(require("../controllers/hit.controller"));

var router = (0, _express.Router)(); // GET

router.get("/hits", _hit["default"].getHits); //DELETE

router["delete"]('/hit/:id', _hit["default"].deleteHit); // ALL

router.all("*", function (req, res) {
  res.status(404).json({
    message: "La ruta de la solicitud HTTP no es reconocida por el servidor."
  });
});
var _default = router;
exports["default"] = _default;