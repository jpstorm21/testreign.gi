"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var eliminatedSchema = new _mongoose.Schema({
  hit_id: String
});

var _default = (0, _mongoose.model)('eliminatedModel', eliminatedSchema);

exports["default"] = _default;