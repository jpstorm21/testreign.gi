"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var hitSchema = new _mongoose.Schema({
  object_id: String,
  title: String,
  url: String,
  author: String,
  created_at: String
});

var _default = (0, _mongoose.model)('hitModel', hitSchema);

exports["default"] = _default;