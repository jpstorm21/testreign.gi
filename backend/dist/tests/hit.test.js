"use strict";

var request = require("supertest");

var chai = require("chai");

var server = require("../../dist/server")["default"];

var _require = require('../../dist/helpers/helpers'),
    simulateDeleteHit = _require.simulateDeleteHit;

var expect = chai.expect;
/**
 * Testing get all hits endpoint
 */

describe("GET /hits", function () {
  it("respond with json containing a list of all hits", function (done) {
    request(server).get("/api/hits").expect("Content-Type", /json/).expect(200, done);
  });
});
/**
 * Testing deleting an existing hit
 */

describe("DELETE /hit/:id", function () {
  it("respond with json containing a msg with 'hit eliminado exitosamente'", function (done) {
    // function that simulate endpoint delete
    var _simulateDeleteHit = simulateDeleteHit('24501200'),
        status = _simulateDeleteHit.status,
        msg = _simulateDeleteHit.msg;

    expect(status).to.equal(200);
    expect(msg).to.equal("hit eliminado exitosamente");
    done(); // request(server)
    //   .delete("/api/hit/24500402")
    //   .set("Accept", "application/json")
    //   .end((err, res) => {
    //     expect("Content-Type", /json/);
    //     expect(res.statusCode).to.equal(200);
    //     expect(res.body.msg).to.equal("hit eliminado exitosamente");
    //     if (err) return done(err);
    //     done();
    //   });
  });
  it("respond with json containing a msg with 'Hit no existe' when the hit does not exists", function (done) {
    request(server)["delete"]("/api/hit/11111").set("Accept", "application/json").end(function (err, res) {
      expect("Content-Type", /json/);
      expect(res.statusCode).to.equal(404);
      expect(res.body.msg).to.equal("Hit no existe");
      if (err) return done(err);
      done();
    });
  });
});