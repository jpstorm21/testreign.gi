"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _hit = _interopRequireDefault(require("../models/hit"));

var _eliminated = _interopRequireDefault(require("../models/eliminated"));

var hitCtrl = {};

hitCtrl.getHits = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _hit["default"].find().sort({
              created_at: -1
            });

          case 3:
            data = _context.sent;
            return _context.abrupt("return", res.status(200).json(data));

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            res.status(500).json({
              msg: "Ocurrió un error en el servidor.",
              error: _context.t0
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

hitCtrl.deleteHit = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var id, hit, hitEliminated;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            id = req.params.id;
            _context2.next = 4;
            return _hit["default"].findOne({
              object_id: id
            });

          case 4:
            hit = _context2.sent;

            if (hit) {
              _context2.next = 7;
              break;
            }

            return _context2.abrupt("return", res.status(404).json({
              msg: "Hit no existe"
            }));

          case 7:
            hitEliminated = new _eliminated["default"]({
              hit_id: hit.object_id
            });
            _context2.next = 10;
            return hitEliminated.save();

          case 10:
            _context2.next = 12;
            return _hit["default"].findByIdAndDelete(hit._id);

          case 12:
            return _context2.abrupt("return", res.status(200).json({
              msg: "hit eliminado exitosamente"
            }));

          case 15:
            _context2.prev = 15;
            _context2.t0 = _context2["catch"](0);
            res.status(500).json({
              msg: "Ocurrió un error en el servidor.",
              error: _context2.t0
            });

          case 18:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 15]]);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = hitCtrl;
exports["default"] = _default;