"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.URI = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _process$env = process.env,
    MONGO_NAME = _process$env.MONGO_NAME,
    MONGO_HOST = _process$env.MONGO_HOST;
console.log(MONGO_NAME);
var URI = process.env.MONGOOSE_URI ? process.env.MONGOOSE_URI : "mongodb://localhost/testReign";
exports.URI = URI;

_mongoose["default"].connect(URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var connection = _mongoose["default"].connection;
connection.once('open', function () {
  console.log('Database is connected');
});