"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.simulateDeleteHit = exports.loadData = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _axios = _interopRequireDefault(require("axios"));

var _hit2 = _interopRequireDefault(require("../models/hit"));

var _eliminated = _interopRequireDefault(require("../models/eliminated"));

var loadData = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
    var response, eliminateds, hits;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _hit2["default"].remove({});

          case 2:
            _context2.next = 4;
            return _axios["default"].get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");

          case 4:
            response = _context2.sent;
            _context2.next = 7;
            return _eliminated["default"].find();

          case 7:
            eliminateds = _context2.sent;

            if (response.status === 200) {
              hits = response.data.hits;
              hits.forEach( /*#__PURE__*/function () {
                var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(hit) {
                  var created_at, title, url, author, story_title, story_url, objectID, eliminated, _hit;

                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          created_at = hit.created_at, title = hit.title, url = hit.url, author = hit.author, story_title = hit.story_title, story_url = hit.story_url, objectID = hit.objectID;

                          if (!(title || story_title)) {
                            _context.next = 7;
                            break;
                          }

                          eliminated = eliminateds.find(function (eliminated) {
                            return eliminated.hit_id === objectID;
                          });

                          if (eliminated) {
                            _context.next = 7;
                            break;
                          }

                          _hit = new _hit2["default"]({
                            title: title || story_title,
                            url: url || story_url,
                            object_id: objectID,
                            author: author,
                            created_at: created_at
                          });
                          _context.next = 7;
                          return _hit.save();

                        case 7:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x) {
                  return _ref2.apply(this, arguments);
                };
              }());
            }

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function loadData() {
    return _ref.apply(this, arguments);
  };
}();

exports.loadData = loadData;

var simulateDeleteHit = function simulateDeleteHit(id) {
  var aux = [];
  /* array that simulate a find in hitModels (
        const hit = await hitModel.findOne({object_id: id});
      ) */

  var hits = [{
    _id: "5f62f1108296e2229c6d0bcd",
    title: "VMware Cuts Pay for Remote Workers Fleeing Silicon Valley",
    url: "https://finance.yahoo.com/news/vmware-twitter-cut-pay-remote-184722278.html",
    object_id: "24501200",
    author: "amscanne",
    created_at: "2020-09-17T04:31:39.000Z"
  }];
  var hit = hits.find(function (hit) {
    return hit.object_id === id;
  });

  if (!hit) {
    return res.status(404).json({
      msg: "Hit no existe"
    });
  }
  /* simulate save in bd
        const hitEliminated = new eliminatedModel({ hit_id: hit.object_id });
        await hitEliminated.save();
    */


  aux.push(hit);
  /* simulate delete in bd
       await hitModel.findByIdAndDelete(hit._id);
    */

  aux.filter(function (x) {
    return x !== hit._id;
  });
  return {
    status: 200,
    msg: "hit eliminado exitosamente"
  };
};

exports.simulateDeleteHit = simulateDeleteHit;