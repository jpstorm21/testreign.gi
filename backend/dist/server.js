"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _morgan = _interopRequireDefault(require("morgan"));

var _nodeCron = _interopRequireDefault(require("node-cron"));

var _hit = _interopRequireDefault(require("./routes/hit.routes"));

var _helpers = require("./helpers/helpers");

require("./helpers/connectionMongoose");

require('dotenv').config();

var app = (0, _express["default"])();
app.set("port", process.env.PORT || 4000);
app.use((0, _morgan["default"])('dev'));
app.use((0, _cors["default"])());
app.use(_express["default"].json()); //routes API

app.use('/api', _hit["default"]);

_nodeCron["default"].schedule('* * */1 * *', function () {
  (0, _helpers.loadData)();
});

app.use('/', function (req, res) {
  res.send('API EXPRESS');
});
app.listen(app.get("port"), function () {
  console.log("Server on port ".concat(app.get("port")));
});
var _default = app;
exports["default"] = _default;