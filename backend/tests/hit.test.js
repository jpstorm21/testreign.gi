const request = require("supertest");
const chai = require("chai")
const server = require("../src/server").default;

const { simulateDeleteHit } = require('../src/helpers/helpers');

const expect = chai.expect;

/**
 * Testing get all hits endpoint
 */
describe("GET /hits", () => {
  it("respond with json containing a list of all hits", (done) => {
    request(server)
      .get("/api/hits")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});

/**
 * Testing deleting an existing hit
 */
describe("DELETE /hit/:id", () => {
  it("respond with json containing a msg with 'hit eliminado exitosamente'", (done) => {
    
    // function that simulate endpoint delete
    const { status, msg } = simulateDeleteHit('24501200');

    expect(status).to.equal(200);
    expect(msg).to.equal("hit eliminado exitosamente");
    done();
    
    // how to in the real endopoint
    
    // request(server)
    //   .delete("/api/hit/24500402")
    //   .set("Accept", "application/json")
    //   .end((err, res) => {
    //     expect("Content-Type", /json/);
    //     expect(res.statusCode).to.equal(200);
    //     expect(res.body.msg).to.equal("hit eliminado exitosamente");
    //     if (err) return done(err);
    //     done();
    //   });
  });

  it("respond with json containing a msg with 'Hit no existe' when the hit does not exists", (done) => {
    request(server)
      .delete("/api/hit/111")
      .set("Accept", "application/json")
      .end((err, res) => {
        expect("Content-Type", /json/);
        expect(res.statusCode).to.equal(404);
        expect(res.body.msg).to.equal("Hit no existe");
        if (err) return done(err);
        done();
      });
  });
});

/**
 * Testing not found route
 */
describe("GET /hits", () => {
  it("respond with json containing a message", (done) => {
    request(server)
      .delete("/api/not")
      .set("Accept", "application/json")
      .end((err, res) => {
        expect("Content-Type", /json/);
        expect(res.statusCode).to.equal(404);
        expect(res.body.message).to.equal("La ruta de la solicitud HTTP no es reconocida por el servidor.");
        if (err) return done(err);
        done();
      });
  });
});