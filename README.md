# TEST REIGN

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd backend
$ npm install
$ npm run dev
```
Install the dependencies and devDependencies and start the client.

```sh
$ cd frontend
$ npm install
$ npm start
```

### TEST AND COVERAGE

run only test

```sh
$ cd backend
$ npm run test
```

run test + coverage

```sh
$ cd backend
$ npm run coverage
```

### Run Docker Backend

```sh
cd frontend
npm run start-container
```

### Run Docker Frontend

```sh
cd backend
npm run start-container
```
