import {
  ERROR_HITS,
  RECIVE_HITS,
  SUCCESS_HITS,
  DELETE_HITS,
} from "../actions/hits";

export default (
  state = {
    data: [],
    reciveRequest: false,
    errorRequest: false,
    deleteReady: false,
  },
  action
) => {
  switch (action.type) {
    case RECIVE_HITS:
      return {
        ...state,
        reciveRequest: true,
      };
    case SUCCESS_HITS:
      return {
        ...state,
        reciveRequest: false,
        deleteReady: false,
        data: action.data,
      };
    case DELETE_HITS:
      return {
        ...state,
        reciveRequest: false,
        deleteReady: true,
      };
    case ERROR_HITS:
      return {
        ...state,
        reciveRequest: false,
        errorRequest: true
      };
    default:
      return state;
  }
};
