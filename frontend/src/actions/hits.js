import axios from "axios";
import { URL } from "../config.json";

// hits types
export const RECIVE_HITS = "RECIVE_HITS";
export const SUCCESS_HITS = "SUCCESS_HITS";
export const DELETE_HITS = "DELETE_HITS";
export const ERROR_HITS = "ERROR_HITS";

const reciveHits = () => {
  return {
    type: RECIVE_HITS,
  };
};

const successHits = (data) => {
  return {
    type: SUCCESS_HITS,
    data,
  };
};

const deleteHits = () => {
  return {
    type: DELETE_HITS,
  };
};

const errorHits = () => {
  return {
    type: ERROR_HITS,
  };
};

export const getHits = () => async (dispatch) => {
  try {
    dispatch(reciveHits());
    const response = await axios.get(`${URL}api/hits`);

    const { status } = response;
    if (status === 200) {
      const { data } = response;
      dispatch(successHits(data));
    } else {
      dispatch(errorHits());
    }
  } catch (error) {
    dispatch(errorHits());
  }
};

export const deleteHit = (id) => async (dispatch) => {
  try {
    dispatch(reciveHits());
    const response = await axios.delete(`${URL}api/hit/${id}`);

    const { status } = response;
    if (status === 200) {
      dispatch(deleteHits());
    } else {
      dispatch(errorHits());
    }
  } catch (error) {
    dispatch(errorHits());
  }
};
