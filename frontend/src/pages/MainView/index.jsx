import React from "react";
import { AppBar, List } from "../../components";
import { Container } from "./styles";

const MainView = () => {
  return (
    <>
      <AppBar />
      <Container>
        <List />
      </Container>
    </>
  );
};

export default MainView;
