import React, { useEffect, useCallback, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useSnackbar } from "notistack";
import { LoadingSnackbar } from "../../components";
import { getHits } from "../../actions/hits";
import { Item } from "../../components";

const List = () => {
  const { data, reciveRequest, errorRequest, deleteReady } = useSelector(
    (state) => state.hits
  );
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const initialSnackbarRef = useRef();

  useEffect(() => {
    if (reciveRequest) {
      initialSnackbarRef.current = enqueueSnackbar("Cargando...", {
        variant: "default",
        persist: true,
        content: (key, message) => (
          <LoadingSnackbar id={key} message={message} />
        ),
      });
    }
    return () => {
      closeSnackbar(initialSnackbarRef.current);
    };
  }, [reciveRequest, enqueueSnackbar, closeSnackbar]);

  const doRequest = useCallback(async () => {
    dispatch(getHits());
  }, [dispatch]);

  useEffect(() => {
    if (errorRequest) {
      enqueueSnackbar("Error al obtener los datos del servidor.", {
        variant: "error",
      });
    }
  }, [errorRequest, enqueueSnackbar]);

  useEffect(() => {
    doRequest();
  }, [doRequest]);

  useEffect(() => {
    if (deleteReady) {
      doRequest();
    }
  }, [deleteReady, doRequest]);

  return (
    <>
      {data.map((hit, index) => (
        <Item
          key={index}
          title={hit.title}
          author={hit.author}
          date={hit.created_at}
          id={hit.object_id}
          url={hit.url}
        />
      ))}
    </>
  );
};

export default List;
