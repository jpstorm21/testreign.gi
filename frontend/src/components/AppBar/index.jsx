import React from "react";
import { Toolbar } from "@material-ui/core";
import { StyledAppBar, StyledTitle, StyledSubTitle } from "./styles";

const AppBar = () => {
  return (
    <>
      <StyledAppBar position="fixed">
        <Toolbar>
          <StyledTitle>HN Feed</StyledTitle>
        </Toolbar>
        <StyledSubTitle>{"We <3 hackers news!!"}</StyledSubTitle>
      </StyledAppBar>
      <Toolbar />
    </>
  );
};

export default AppBar;
