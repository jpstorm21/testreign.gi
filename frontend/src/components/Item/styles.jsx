import styled from "styled-components";

export const FileInputWrapper = styled.span`
  display: flex;
  flex-direction: row;
  width: 100%;
  background-color: #fff;
  border: 1px #ccc;
  &:hover {
    background-color: #fafafa;
  }
`;

export const Title = styled.h2`
  color: #333;
  font-size: 13pt;
`;

export const Author = styled.h2`
  color: #999;
  font-size: 13pt;
`;

export const Time = styled.h2`
  color: #333;
  font-size: 13pt;
`;

export const StyledButton = styled.span`
  padding-left: 1rem;
  padding-right: 1rem;
  background-color: ${({ theme }) => theme.palette.primary.main};
  color: ${({ theme }) => theme.palette.secondary.contrastText};
  padding: 6px 16px;
  font-size: 0.875rem;
  min-width: 64px;
  box-sizing: border-box;
  transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
    box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
    border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 500;
  line-height: 1.75;
  border-radius: 4px;
  letter-spacing: 0.02857em;
  text-transform: uppercase;
  border: 0;
  cursor: pointer;
  margin: 0;
  display: inline-flex;
  outline: 0;
  position: relative;
  align-items: center;
  user-select: none;
  vertical-align: middle;
  -moz-appearance: none;
  justify-content: center;
  text-decoration: none;
  -webkit-tap-highlight-color: transparent;
  max-height: 3rem !important;
  align-self: center !important;
  margin-left: 2rem !important;
  &:hover {
    background-color: red;
  }
`;

export const LeftWrapper = styled.span`
  flex: 3;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  cursor: pointer;
  .MuiCheckbox-root {
    color: ${({ theme }) => theme.palette.primary.contrastText};
  }
`;

export const RightWrapper = styled.span`
  display: flex;
  flex: 3;
  justify-content: flex-end;
`;
