import React from "react";
import PropTypes from "prop-types";
import { Divider, Tooltip } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { Delete } from "@material-ui/icons";
import { deleteHit } from "../../actions/hits";
import {
  FileInputWrapper,
  StyledButton,
  LeftWrapper,
  RightWrapper,
  Author,
  Time,
  Title,
} from "./styles";

const Item = ({ title, author, date, id, url }) => {
  const dispatch = useDispatch();
  const fecha = new Date(date).toLocaleDateString();

  const handleClick = () => {
    dispatch(deleteHit(id));
  };

  const goToUrl = () => {
    window.open(url);
  };

  return (
    <>
      <FileInputWrapper>
        <LeftWrapper onClick={goToUrl}>
          <Title>{title} -</Title>
          <Author>{author} -</Author>
        </LeftWrapper>
        <RightWrapper>
          <Time>{fecha}</Time>
          <StyledButton onClick={handleClick}>
            <Tooltip title="Eliminar">
              <Delete />
            </Tooltip>
          </StyledButton>
        </RightWrapper>
      </FileInputWrapper>
      <Divider />
    </>
  );
};

Item.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};

export default Item;
